魁北克同等学历认证 - FLYabroad
===============================

魁北克同等学历认证（Évaluation comparative des études effectuées hors du Québec）是魁北克移民部（MICC）提供的一项对国外学历进行认证的服务，认证的结果用于将国外学历对应到具体的魁北克学历（相当于是魁北克的什么学历），包括入学要求，学历，年限，专业等，具体到请参考上面的认证样本。

魁北克学历对等认证适用群体
----------------------------

学历对等认证服务是魁北克移民部推出，面向的群体主要是移民和打算到魁北克工作的临时工人或留学生。如果打算在魁北克从事教师或公共服务（公务员）职业就必须要有对等认证，其他有些雇主，限制性行业及学校也可能要求必须有对等学历转换。同时对等学历转换对于移民申请人确定受训加分情况很有参考作用。因此打算去魁省工作、学习或者定居的人们做好了学历对等认证会比较方便。

1. 11年2月-12年3月间递交CSQ申请还没有拿到CSQ的申请人，受训不是非常明确加分的，不知道考试或面试需要达到的水平的
2. 12年3月份后递交的申请没有收到档案号的申请人（收到档案号的话说明受训是认可的）
3. 以上两种情况涉及到副申有可能加分的
4. 打算移民魁北克，但受训不是非常明确的潜在申请人

FLYabroad 魁北克学历对等转换服务
----------------------------------

经过认证的学历会体现相当于魁省什么level及年限的学习以及相当其哪个专业（受训），具体可以参考认证样本，这无疑对受训的认定非常有帮助。如果您的专业受训不确定或者希望自己在面试时有更大的信心可以获得受训加分，或者不确定自己的受训能否加分因此不知道法语达到什么程度才应该提交成绩的申请人来说很有意义。

为了方便已经提交和打算提交魁北克技术移民申请的申请人们，FLYabroad 推出魁北克对等学历转换服务，当前该服务的申请周期较长，大概需要半年，有需要的建议尽快准备。

申请魁省学历认证需要的资料清单
--------------------------------------------------
 
* 身份证明 （护照，出生公证，CSQ，登陆纸，公民卡，枫叶卡，曾用名公证）
* 大学毕业证，学位证原件 
* 成绩单原件/密封件 

**申请注意事项：** 学历学位证书的翻译必须由魁省的指定的翻译会员翻译或者确认才被认可，翻译部分 FLYabroad 会帮您完成，但翻译费用需要您自行支付，当前正规有场所的翻译大概收费在30加币，小广告的翻译收费在15-25加币，稳妥起见 FLYabroad 会选择正规翻译。

FLYabroad 魁北克学历对等转换服务费用
----------------------------------------

对于已签约FLYabroad的客户，服务费为1000元人民币；
对于非 FLYabroad 魁北克签约客户，服务费2000元人民币。该费用一次性提前支付，可以通过支付宝或银行转账。

以上为 FLYabroad 纯服务费用，第三方费用申请人自理：包括来回的国际快递费（大概2-3次），每份文件的翻译费 30加元/份，申请费（112加币）。以实收为准。对于申请费申请人可以将等值的人民币费用支付给 FLYabroad ，由 FLYabroad 代为支付给移民部。

对于没有递交申请的申请人，如果认证后委托FLYabroad办理全程，可以减免1000元服务费。

魁北克移民部同等学历认证过程中不被评估的资料
--------------------------------------------

Documents that are not evaluated：

* Academic documents certifying a program that is not recognized by authorities of the province or country where the studies were done 非教育部或教育部认可学校的证明
* Academic documents issued by an educational institution that is not accredited by authorities of the province or country where the studies
were done 民办学历
* Academic documents that certify primary or secondary school studies of less than nine years duration (excluding kindergarten) 不足九年的中小学教育证明
* Certificate of school attendance or registration 在读或入学证明
* Attestations or certificates of competency or professional qualification 能力或专业资格证明
* Work certificates 职业证书
* Certificates of work internships that are not part of official academic training leading to a diploma 教学要求范围以外的实习工作证明
* Transcripts or diplomas for studies lasting less than one academic year 不足一年的学历或成绩单
* Transcripts for studies not successfully completed 未完成学习的成绩单
* Copies not certified true by the educational institution or by authorities of the province or country where the studies were done 没有被认证的复印件或副本
* Documents sent by fax or e-mail 不接受传真和email资料
* Non-standard documents or documents that have been changed, corrected or altered. 非正规文档或有修改的文档